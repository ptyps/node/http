"use strict"

const {pathToRegexp: ptr} = require('path-to-regexp')
const hsc = require('http-status-codes')
const mtypes = require('mime-types')
const events = require('events')
const net = require('net')
const fs = require('fs')

module.exports.METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
  PATCH: 'PATCH',
  HEAD: 'HEAD',
  TRACE: 'TRACE',
  OPTIONS: 'OPTIONS',
  CONNECT: 'CONNECT'
}

module.exports.CODE = {
  NOT_ACCEPTABLE: hsc.NOT_ACCEPTABLE,
  UNAUTHORIZED: hsc.UNAUTHORIZED,
  FORBIDDEN: hsc.FORBIDDEN,
  NOT_FOUND: hsc.NOT_FOUND,
  SERVER_ERROR: 500,
  OK: hsc.OK
}

function Client(socket) {
  if (!(this instanceof Client))
    return new Client(socket)

  this.__proto__ = events.EventEmitter.prototype

  let buffer = ''

  socket.on('data', data => {
    buffer = buffer += data;

    if (!buffer.endsWith('\r\n'))
      return

    let [top, body] = buffer.split('\r\n\r\n')

    let [head] = top.split('\r\n', 1)

    let [, method, path, proto, version] = head.match(/(.*) (.*) (.*)\/(.*)/)

    let request = {
      body, version, url: path,
      
      method: method.toLowerCase(),

      get protocol() {
        let [, p1] = top.match(/X-Forwarded-Proto: (.*)/)
        return p1 || proto.toLowerCase()
      },

      get host() {
        let [, , h1] = top.match(/(Host|X-Forwarded-Host|X-Host): (.*)/)
        return h1
      },

      get ip() {
        let [, i1] = top.match(/X-Forwarded-For: (.*)/)
        return i1 || socket.remoteAddress
      },

      get agent() {
        let [, u1] = top.match(/User-Agent: (.*)/)
        return u1
      }
    }

    let respond = {
      out: (body, opts = {}) => new Promise(res => {
        socket.once('end', res)

        if (!opts.code)
          opts.code = hsc.OK

        if (!opts.type)
          opts.type = 'text/plain'

        let status = hsc.getStatusText(opts.code)

        socket.write(`${proto}/${version} ${opts.code} ${status}\n`)
        socket.write(`Content-Type: ${opts.type}\n`)
        socket.write("\n")
        socket.write(body)
        socket.end()
      }),

      file: function(path, opts = {}) {
        return new Promise((res, rej) => {
          fs.readFile(path, (err, buffer) => {
            if (err)
              return rej(err)

            buffer = buffer.toString()

            res(buffer)
          })
        })

        .then(buffer => this.out(buffer, Object.assign(opts, {
          type: mtypes.lookup(path)
        })))
      }
    }

    this.emit('request', request, respond)
  })

  socket.once('end', () => this.emit('close'))

  Object.assign(this, {
    out: data => socket.write(data)
  })
}

module.exports.Server = function Server() {
  if (!(this instanceof Server))
    return new Server()

  this.__proto__ = events.EventEmitter.prototype

  let server = new net.createServer()

  server.on('connection', socket => {
    let incoming = new Client(socket)

    incoming.on('request', (req, res) => {
      this.emit('request', req, res)
    })
  })

  Object.assign(this, {
    listen: (...args) => server.listen.apply(server, args)
  })
}

module.exports.Router = function Router() {
  if (!(this instanceof Router))
    return new Router()

  let handles = []

  function Route(request, respond, then) {
    let list = handles.filter(handle => {
      if (handle.method && handle.method != request.method)
        return !1

      let matches = handle.pattern.exec(request.url)

      if (!matches)
        return !1

      return !0
    })

    // console.log(list)

    let run = () => new Promise((res, rej) => {
      let next = err => {
        if (err)
          return rej(err)

        let up = list.shift()

        if (!up)
          return res()

        up.fn(request, respond, {
          remove: up.remove,
          next
        })
      }

      next()
    })

    run().then(() => {
      if (then)
        return then.next()

      return respond.out(`No such path found: ${request.url}`, {code: hsc.NOT_FOUND})
    }, err => {

    })
  }

  return Object.assign(Route, {
    on: function(method, url, fn) {
      let keys = []

      handles.push({
        method: !method ? null : method.toLowerCase(),
        pattern: ptr(url, keys),

        fn: !fn.on ? fn : (request, respond, then) => {
          request.url = request.url.replace(url, '')

          if (!request.url.startsWith('/'))
            request.url = `/${request.url}`

          fn(request, respond, then)
        },

        remove: function() {
          let i = handles.indexOf(this)
          handles.splice(i, 1)
        },

        get keys() { 
          return keys
        }
      })
    },

    use: function(fn) {
      this.on(null, '(.*)', fn)
    },

    with: function(url, fn) {
      this.on(null, url, fn)
    },

    get: function(url, fn) {
      return this.on('GET', url, fn)
    },

    post: function(url, fn) {
      return this.on('POST', url, fn)
    },

    put: function(url, fn) {
      return this.on('PUT', url, fn)
    },

    delete: function(url, fn) {
      return this.on('DELETE', url, fn)
    },

    patch: function(url, fn) {
      return this.on('PATCH', url, fn)
    },

    head: function(url, fn) {
      return this.on('HEAD', url, fn)
    },

    trace: function(url, fn) {
      return this.on('TRACE', url, fn)
    },

    options: function(url, fn) {
      return this.on('OPTIONS', url, fn)
    },

    connect: function(url, fn) {
      return this.on('CONNECT', url, fn)
    }
  })
}
